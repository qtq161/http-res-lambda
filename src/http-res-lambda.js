module.exports = (statusCode, body) => {
	const response = {
		statusCode,
	}
	if (body) {
		const isStr = typeof body === 'string'
		response.body = isStr ? body : JSON.stringify(body)
		response.headers = {
			'Content-Type': (isStr ? 'text/plain' : 'application/json')
			+ '; charset=utf-8'
		}
	}
	return response
}
