const assert = require('assert')

const res = require('../dist/http-res-lambda.js')

// Test sending just a statusCode
assert.deepStrictEqual(res(200), {statusCode: 200}, 'Sends status code')

// Test sending objects as JSON
assert.deepStrictEqual(
	res(200, {a:1}),
	{
		statusCode: 200,
		body: JSON.stringify({a:1}),
		headers: {
			'Content-Type': 'application/json; charset=utf-8'
		},
	},
	'Sends status code and object body as JSON'
)

// Test sending plain text
assert.deepStrictEqual(
	res(200, "Just plain text"),
	{
		statusCode: 200,
		body: "Just plain text",
		headers: {
			'Content-Type': 'text/plain; charset=utf-8'
		},
	}
)
